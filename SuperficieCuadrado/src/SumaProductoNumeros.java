import java.util.Scanner;

public class SumaProductoNumeros {
    public static void main(String[] ar){
        Scanner captura = new Scanner(System.in);
        int num1, num2;
        //
        System.out.print("Introduzca el primer número:");
        num1 = captura.nextInt();
        System.out.print("Introduzca el segundo número:");
        num2 = captura.nextInt();
        //
        int suma = num1 + num2;
        System.out.println("La suma de ambos números es: " + suma);
        //
        int producto = num1 * num2;
        System.out.println("El producto de ambos números es: " + producto);
    }
}
