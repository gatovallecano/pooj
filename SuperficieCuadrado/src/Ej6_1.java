import java.util.Scanner;

public class Ej6_1 {
    public static void main(String[] ar){
        //final int dinero = 10;
        Scanner captura = new Scanner(System.in);
        int lado, perimetro;
        System.out.print("Ingresa el lado de un cuadrado: ");
        lado = captura.nextInt();
        perimetro = lado * 4;
        System.out.println("El perímetro es " + perimetro);
    }
}
