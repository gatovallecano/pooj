/*import java.util.*;*/
public class Tabla2 {
    public static void main(String[] ar) {
        int[] datos = {3, 2, 6, 8, 4, 1, 7, 5};
        int cnt,cnt_gral = 0;
        do{
            cnt_gral++;
            cnt = 0;
            for(int i=0;i<datos.length-1;i++){
                if(datos[i] < datos[i+1]){
                    cnt++;
                } else {
                    int aux = datos[i+1];
                    datos[i+1] = datos[i];
                    datos[i] = aux;
                }
            }
        } while (cnt < datos.length-1);

        for(int dato:datos){
            System.out.println(dato);
        }
        System.out.println("Vueltas necesarias --> "+cnt_gral);
    }
}
