import java.util.Scanner;

public class SuperficieCuadrado {
    public static void main(String[] ar ){
        Scanner teclado = new Scanner(System.in);
        int lado;
        int superficie;
        System.out.println("Introduzca el valor del lado del cuadrado");

        lado=teclado.nextInt();

        superficie = lado * lado;
        System.out.println("La superficie del cuadrado es:");
        System.out.println(superficie);
    }
}
