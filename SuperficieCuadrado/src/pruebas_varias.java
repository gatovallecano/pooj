public class pruebas_varias {
    public static void main(String[] ar) {
        final String salida = "Hola\nCaracola \b";
        System.out.println(salida);

        cliente_prueba Cliente1 = new cliente_prueba();
        cliente_prueba Cliente2 = new cliente_prueba();
        cliente_prueba Cliente3 = new cliente_prueba();

        Cliente2.setId(2);
        Cliente2.setnombre("Antonio");

        Cliente3.setId(3);
        Cliente3.setnombre("Ceferino");

        cliente_prueba[] Clientes = new cliente_prueba[3];

        Clientes[0] = Cliente1;
        Clientes[1] = Cliente2;
        Clientes[2] = Cliente3;

        for (cliente_prueba item: Clientes) {
            System.out.println("id: "+item.getId()+"\tNombre: "+item.getnombre());
        }
    }
}
