public class Villanos extends Humanos {
    String colorBigote;
    String colorSombrero;
    String aspecto;
    int gradoEbriedad;
    int damiselasSecuestradas;
    Humanos damisela;

    public Villanos(){
        aspecto = "malo";
        gradoEbriedad = 0;
        damiselasSecuestradas = 0;
    }
    public void tomarWhiskey(){
        gradoEbriedad++;
    }
    public int cuantoEbrioEstoy(){
        return gradoEbriedad;
    }
    public void secuestrarDamisela(Humanos damisela){
        this.damisela = damisela;
        damiselasSecuestradas++;
        System.out.println("El villano ha secuestrado a " + damisela.comoTeLlamas());
    }
}
