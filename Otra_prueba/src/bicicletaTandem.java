public class bicicletaTandem extends bicicleta{
    private int numAsientos;

    public bicicletaTandem(int velocidad, int plato, int pinon){
        super(velocidad, plato, pinon);
    }

    public void acelerar(){
        this.velocidadActual = super.velocidadActual * 4;
    }

}
