public class bicicleta extends vehiculo {

    protected int velocidadActual;
    protected int platoActual;
    protected int pinonActual;

    public bicicleta(int velocidad, int plato, int pinon){
        velocidadActual = velocidad;
        platoActual = plato;
        pinonActual = pinon;
    }
    public bicicleta(int plato, int pinon){
        platoActual = plato;
        pinonActual = pinon;
    }

    public int getVelocidadActual(){
        return velocidadActual;
    }
    public void setVelocidadActual(int Velocidad){
        this.velocidadActual = Velocidad;
    }

    public int getPlatoActual(){
        return platoActual;
    }
    public void setPlatoActual(int Plato){
        this.platoActual = Plato;
    }

    public int getPinonActual(){
        return pinonActual;
    }
    public void setPinonActual(int Pinon){
        this.pinonActual = Pinon;
    }


    public String color(){
        return "amarillo";
    }

    public void acelerar(){
        velocidadActual++;
    }
    public void frenar(){
        velocidadActual--;
    }
    public void cambiarPlato(){
        platoActual = 1;
    }
    public void cambiarPlato(int Plato){
        platoActual = Plato;
    }
    public void cambiarPinon(int Pinon){
        pinonActual = Pinon;
    }
    public void cambiarPinon(){
        pinonActual = 1;
    }
}
