public class bicicletaMontana extends bicicleta {
    private int suspension;

    public bicicletaMontana(int velocidad, int plato, int pinon){
        super(velocidad, plato, pinon);
        suspension = 0;
    }

    public void cambiarSuspension(int suspension){
        this.suspension = suspension;
    }

    public void acelerar(){
        this.velocidadActual = super.velocidadActual * 3;
    }
}
